CREATE TABLE IF NOT EXISTS "orders" (
    "id" SERIAL PRIMARY KEY NOT NULL,
    "user_id" BIGINT NOT NULL REFERENCES users(id),
    "order_number" BIGINT NOT NULL,
    "total_amount" DECIMAL(18, 2) NOT NULL,
    "region" VARCHAR(255) NOT NULL,
    "district" VARCHAR(255) NOT NULL,
    "address" VARCHAR(255) NOT NULL,
    "status" VARCHAR(255) CHECK ("status" IN('new', 'confirmed', 'delivering', 'finished', 'cancelled')) NOT NULL,
    "payment_status" VARCHAR(255) CHECK ("payment_status" IN('paid', 'unpaid')) NOT NULL,
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL
);


CREATE TABLE IF NOT EXISTS "order_items"(
    "id" SERIAL PRIMARY KEY NOT NULL,
    "book_id" BIGINT NOT NULL REFERENCES books(id),
    "order_id" BIGINT NOT NULL REFERENCES orders(id),
    "product_code" BIGINT NOT NULL,
    "price" DECIMAL(18, 2) NOT NULL,
    "count" BIGINT NOT NULL,
    "total_price" DECIMAL(18, 2),
    "created_at" TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL
);
