package main

import (
	"fmt"
	"log"
	"net"

	_ "github.com/lib/pq"
	"github.com/redis/go-redis/v9"
	"google.golang.org/grpc"

	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/order_service/config"
	grpcclient "gitlab.com/book_market/order_service/pkg/grpc_client"
	"gitlab.com/book_market/order_service/pkg/logger"
	pb "gitlab.com/book_market/order_service/genproto/order_service"
	"gitlab.com/book_market/order_service/service"
	"gitlab.com/book_market/order_service/storage"
)

func main() {
	cfg := config.Load(".")

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)
	
	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}
	
	rdb := redis.NewClient(&redis.Options{
		Addr: cfg.Redis.Addr,
	})

	strg := storage.NewStoragePg(psqlConn)
	inMemory := storage.NewInMemoryStorage(rdb)

	logrus := logger.New()
	grpcClient, err := grpcclient.New(cfg)
	if err != nil {
		logrus.Fatalf("failed to connect another microservice: %v", err)
	}

	orderService := service.NewOrderService(strg, inMemory, logrus, grpcClient)

	lis, err := net.Listen("tcp", cfg.GRPC_PORT)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()

	pb.RegisterOrderServiceServer(s, orderService)

	log.Println("Grpc server started in port ", cfg.GRPC_PORT)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("Error while listening: %v", err)
	}
}
