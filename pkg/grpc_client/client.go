package grpcclient

import (
	"fmt"

	"gitlab.com/book_market/order_service/config"
	pbu "gitlab.com/book_market/book_service/genproto/user_service"
	pb "gitlab.com/book_market/book_service/genproto/book_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)
type GrpcClientI interface {
	UserService() pbu.UserServiceClient
	BookService() pb.BookServiceClient
}

type GrpcClient struct {
	cfg config.Config
	connections map[string]interface{}
}

func New(cfg config.Config) (GrpcClientI, error) {
	connUserService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.UserServiceHost, cfg.UserServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("user service dial host: %s port: %s err: %v",
	cfg.UserServiceHost, cfg.UserServiceGrpcPort, err)
	}

	connBookService, err := grpc.Dial(
		fmt.Sprintf("%s%s", cfg.BookServiceHost, cfg.BookServiceGrpcPort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("book service dial host: %s port: %s err: %v",
	cfg.BookServiceHost, cfg.BookServiceGrpcPort, err)
	}
	return &GrpcClient{
		cfg: cfg,
		connections: map[string]interface{}{
			"user_service": pbu.NewUserServiceClient(connUserService),
			"book_service": pb.NewBookServiceClient(connBookService),
		},
	}, nil
}

func (g *GrpcClient) UserService() pbu.UserServiceClient {
	return g.connections["user_service"].(pbu.UserServiceClient)
}

func (g *GrpcClient) BookService() pb.BookServiceClient {
	return g.connections["book_service"].(pb.BookServiceClient)
}