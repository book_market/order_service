package service

import (
	"context"
	"database/sql"
	"errors"

	"github.com/sirupsen/logrus"
	pbo "gitlab.com/book_market/order_service/genproto/order_service"
	grpcclient "gitlab.com/book_market/order_service/pkg/grpc_client"
	"gitlab.com/book_market/order_service/storage"
	"gitlab.com/book_market/order_service/storage/repo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type OrderService struct {
	pbo.UnimplementedOrderServiceServer
	storage    storage.StorageI
	inMemory   storage.InMemoryStorageI
	logger     *logrus.Logger
	grpcClient grpcclient.GrpcClientI
}

func NewOrderService(strg storage.StorageI, inMemory storage.InMemoryStorageI, log *logrus.Logger, grpcClient grpcclient.GrpcClientI) *OrderService {
	return &OrderService{
		storage:    strg,
		inMemory:   inMemory,
		logger:     log,
		grpcClient: grpcClient,
	}
}

func (o *OrderService) Create(ctx context.Context, req *pbo.Order) (*pbo.Order, error) {
	order, err := o.storage.Order().CreateOrder(&repo.Order{
		UserId:        req.UserId,
		OrderNumber:   req.OrderNumber,
		TotalAmount:   float64(req.TotalAmount),
		Region:        req.Region,
		District:      req.District,
		Address:       req.Address,
		Status:        req.Status,
		PaymentStatus: req.PaymentStatus,
	})
	if err != nil {
		o.logger.WithError(err).Error("failed to create order")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseOrderModel(order), nil
}

func parseOrderModel(order *repo.Order) *pbo.Order {
	return &pbo.Order{
		Id:            order.Id,
		UserId:        order.UserId,
		OrderNumber:   order.OrderNumber,
		TotalAmount:   int64(order.TotalAmount),
		Region:        order.Region,
		District:      order.District,
		Address:       order.Address,
		Status:        order.Status,
		PaymentStatus: order.PaymentStatus,
		CreatedAt:     order.CreatedAt,
	}
}

func (o *OrderService) Get(ctx context.Context, req *pbo.OrderIdRequest) (*pbo.Order, error) {
	order, err := o.storage.Order().GetOrder(req.Id)
	if err != nil {
		o.logger.WithError(err).Error("failed to get order by id")
		if errors.Is(err, sql.ErrNoRows) {
			return nil, status.Errorf(codes.NotFound, err.Error())
		}
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}

	return parseOrderModel(order), nil
}

func (o *OrderService) Delete(ctx context.Context, req *pbo.OrderIdRequest) (*emptypb.Empty, error) {
	err := o.storage.Order().DeleteOrder(req.Id)
	if err != nil {
		o.logger.WithError(err).Error("failed to delete order")
		return nil, status.Errorf(codes.Internal, "Internal server error: %v", err)
	}
	return &emptypb.Empty{}, nil
}