package repo

type Order struct {
	Id            int64
	UserId        int64
	OrderNumber   int64
	TotalAmount   float64
	Region        string
	District      string
	Address       string
	Status        string
	PaymentStatus string
	Items         []*OrderItem
	CreatedAt     string
}

type OrderItem struct {
	Id          int64
	BookId      int64
	OrderId     int64
	ProductCode int64
	Price       float64
	Count       int64
	TotalPrice  float64
	CreatedAt   string
}

type GetAllOrdersParamsByUserId struct {
	Limit  int32
	Page   int32
	UserId int32
}

type GetAllOrdersParams struct {
	Limit  int32
	Page   int32
	Search string
}

type UpdateStatus struct {
	Id     int64
	Status string
}

type GetAllOrdersResults struct {
	Orders []*Order
	Count  int32
}

type OrderStorageI interface {
	CreateOrder(o *Order) (*Order, error)
	GetOrder(id int64) (*Order, error)
	UpdateOrder(o *Order) (*Order, error)
	DeleteOrder(id int64) error
	
}
