package postgres

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/order_service/storage/repo"
)

type orderRepo struct {
	db *sqlx.DB
}

func NewOrder(db *sqlx.DB) repo.OrderStorageI {
	return &orderRepo{
		db: db,
	}
}

func (o *orderRepo) CreateOrder(order *repo.Order) (*repo.Order, error) {
	var orderId int64
	trx, err := o.db.Begin()
	if err != nil {
		return nil, err
	}

	query := `
		insert into orders (
			user_id,
			order_number,
			total_amount,
			region,
			district,
			address,
			status,
			payment_status
		) values ($1, $2, $3, $4, $5, $6, $7, $8)
		returning id, created_at
	`

	row := trx.QueryRow(
		query,
		order.UserId,
		order.OrderNumber,
		order.TotalAmount,
		order.Region,
		order.District,
		order.Address,
		order.Status,
		order.PaymentStatus,
	)

	err = row.Scan(
		&order.Id,
		&order.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	queryInsertItem := `
		insert into order_items (
			book_id,
			order_id,
			product_code,
			price,
			count,
			total_price
		) values ($1, $2, $3, $4, $5, $6)
		returning id, created_at
	`

	for _, item := range order.Items {
		_, err = trx.Exec(
			queryInsertItem,
			item.BookId,
			orderId,
			item.ProductCode,
			item.Price,
			item.Count,
			item.TotalPrice,
		)
		if err != nil {
			trx.Rollback()
			return nil, err
		}
	}
	err = trx.Commit()
	if err != nil {
		return nil, err
	}
	return order, nil
}

func (o *orderRepo) UpdateOrder(order *repo.Order) (*repo.Order, error) {
	var orderId int64
	trx, err := o.db.Begin()
	if err != nil {
		return nil, err
	}

	query := ` update orders set 
					user_id = $1,
					order_number = $2,
					total_amount = $3,
					region = $4,
					district = $5,
					address = $6,
					status = $7,
					payment_status = $8
				where id = $9
				returning id, user_id, order_number, total_amount, region, district, address, status, payment_status`

	row := trx.QueryRow(
		query,
		order.UserId,
		order.OrderNumber,
		order.TotalAmount,
		order.Region,
		order.District,
		order.Address,
		order.Status,
		order.PaymentStatus,
		order.Id,
	)
	err = row.Scan(
		&order.Id,
		&order.UserId,
		&order.OrderNumber,
		&order.TotalAmount,
		&order.Region,
		&order.District,
		&order.Address,
		&order.Status,
		&order.PaymentStatus,
	)
	if err != nil {
		trx.Rollback()
		return nil, err
	}

	queryUpdateItem := `
		update order_items set 
			book_id = $1,
			order_id = $2,
			product_code = $3,
			price = $4,
			count = $5,
			total_price = $6,
		where id = $7
		returning id, book_id, order_id, product_code, price, count, total_price
	`
	for _, item := range order.Items {
		_, err := trx.Exec(
			queryUpdateItem,
			item.BookId,
			orderId,
			item.ProductCode,
			item.Price,
			item.Count,
			item.TotalPrice,
		)
		if err != nil {
			trx.Rollback()
			return nil, err
		}
		order.Items = append(order.Items, item)
	}
	err = trx.Commit()
	if err != nil {
		return nil, err
	}

	return order, nil
}

func (o *orderRepo) GetOrder(id int64) (*repo.Order, error) {
	var (
		result repo.Order
	)
	result.Items = make([]*repo.OrderItem, 0)


	query := `
		select
			o.id,
			o.user_id,
			o.order_number,
			o.total_amount,
			o.region,
			o.district,
			o.address,
			o.status,
			o.payment_status,
			o.created_at
		from orders o
		inner join order_items oi on o.id = oi.order_id 
		where o.id = $1
	`

	row := o.db.QueryRow(query, id)
	err := row.Scan(
		&result.Id,
		&result.UserId,
		&result.OrderNumber,
		&result.TotalAmount,
		&result.Region,
		&result.District,
		&result.Address,
		&result.Status,
		&result.PaymentStatus,
		&result.CreatedAt,
	)
	if err != nil {
		return nil, err
	}

	queryItems := `
		select
			id,
			book_id,
			order_id,
			product_code,
			price,
			count,
			total_price,
			created_at
		from order_items 
		where id = $1
	`

	rows, err := o.db.Query(queryItems, id)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var item repo.OrderItem
		err := row.Scan(
			&item.Id,
			&item.BookId,
			&item.OrderId,
			&item.ProductCode,
			&item.Price,
			&item.Count,
			&item.TotalPrice,
			&item.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		result.Items = append(result.Items, &item)
	}
	return &result, nil
}

func (m *orderRepo) DeleteOrder(id int64) error {
	trx, err := m.db.Begin()
	if err != nil {
		return err
	}

	queryDeleteItem := `
			delete from order_items where id=$1
	`

	_, err = trx.Exec(queryDeleteItem, id)
	if err != nil {
		trx.Rollback()
		return err
	}

		queryDelete := `
			delete from orders where id=$1
	`
		result, err := trx.Exec(queryDelete, id)
		if err != nil {
			trx.Rollback()
			return err
		}
	
		rowsCount, err := result.RowsAffected()
		if err != nil {
			trx.Rollback()
			return err
		}

	if rowsCount == 0 {
		return sql.ErrNoRows
	}
	err = trx.Commit()
	if err != nil {
		return err
	}
	return nil

}