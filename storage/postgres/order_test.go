package postgres_test

import (
	"database/sql"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/book_market/order_service/storage/repo"
)

func createOrder(t *testing.T) *repo.Order {
	var order repo.Order
	c, err := strg.Order().CreateOrder(&repo.Order{
		UserId:        2,
		OrderNumber:   1,
		TotalAmount:   123300,
		Region:        "Andijon",
		District:      "Andijon",
		Address:       "132",
		Status:        "new",
		PaymentStatus: "unpaid",
		Items: []*repo.OrderItem{
			{
				BookId:      5,
				OrderId:     order.Id,
				ProductCode: 123456,
				Price:       10000,
				Count:       2,
				TotalPrice:  20000,
			},
		},
	})
	require.NoError(t, err)
	require.NotEmpty(t, c)

	return c
}

func deleteOrder(t *testing.T, id int64) {
	err := strg.Order().DeleteOrder(id)
	require.NoError(t, err)

}

func TestCreateOrder(t *testing.T) {
	createOrder(t)
}

func TestGetOrder(t *testing.T) {
	c := createOrder(t)

	order, err := strg.Order().GetOrder(c.Id)
	require.NoError(t, err)
	require.NotEmpty(t, order)

	deleteOrder(t, c.Id)
}

func TestDeleteOrder(t *testing.T) {
	c := createOrder(t)
	deleteOrder(t, c.Id)
	err := strg.Order().DeleteOrder(-1)
	require.Error(t, err, sql.ErrNoRows)

}
