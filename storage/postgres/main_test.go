package postgres_test

import (
	"fmt"
	"log"
	"os"
	"testing"
	_ "github.com/lib/pq"

	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/order_service/config"
	"gitlab.com/book_market/order_service/storage"
)


var strg storage.StorageI

func TestMain(m *testing.M) {
	cfg := config.Load("./../..")

	connstr := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	db, err := sqlx.Open("postgres", connstr)
	if err != nil {
		log.Fatalf("failed to open connection: %v", err)
	}

	strg = storage.NewStoragePg(db)
	os.Exit(m.Run())
}