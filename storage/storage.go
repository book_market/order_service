package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/book_market/order_service/storage/postgres"
	"gitlab.com/book_market/order_service/storage/repo"
)

type StorageI interface {
	Order() repo.OrderStorageI
}

type storagePg struct {
	orderRepo     repo.OrderStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		orderRepo:     postgres.NewOrder(db),
	}
}

func (s *storagePg) Order() repo.OrderStorageI {
	return s.orderRepo
}
